## bike -- Calculate Detail Bicycle Gear Ratios

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/bike) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/bike.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/bike.gmi (mirror)

bike(1) will generate a report showing Detail Bicycle
Gear Ratios.

I originally created this in 1975 using FORTRAN IV on a CDC 7500,
I then translated to c in 1993.

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[GNU Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
